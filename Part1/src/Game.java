import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

//This class is the framework used to build future games
public abstract class Game {
	private static Game instance;

	//Create a new game
	public Game() {
		try {
			//Set reference
			instance = this;

			//Create display
			Display.create();
			setDisplayMode(800, 600, false); //Set ideal DisplayMode
			Display.setResizable(true);

			//Start game
			gameLoop();
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(-1); //Stop game
		}

	}

	//Current time in ms
	public static long getCurrentTime(){
		return Sys.getTime() * 1000 / Sys.getTimerResolution();
	}

	//Actual game
	private void gameLoop(){
		long lastFrame = getCurrentTime();
		long thisFrame = getCurrentTime();

		init();

		while (!Display.isCloseRequested()){
			thisFrame = getCurrentTime();
			update(thisFrame - lastFrame);
			render();

			lastFrame = thisFrame;
			Display.update();

			//Check for window being resized
			if(Display.wasResized()){
				resized();
			}

			Display.sync(60); //How many fps to run at
		}

		end();
	}

	public static void end(){ //Close game properly
		instance.dispose();
		Display.destroy();
		System.exit(0);
	}

	//Find best DisplayMode supported by graphics setup
	public static boolean setDisplayMode(int width, int height, boolean fullscreen){
		//Check if already set
		if((Display.getDisplayMode().getWidth() == width) &&
				(Display.getDisplayMode().getHeight() == height) &&
				(Display.isFullscreen() == fullscreen)) 
			return true;

		try {
			//Target DisplayMode
			DisplayMode targetDisplayMode = null;

			if(fullscreen){
				//Get all DisplayModes
				DisplayMode[] modes = Display.getAvailableDisplayModes();
				int freq = 0; //Refresh rate of current DisplayMode

				//Iterate over modes
				for (DisplayMode current : modes){
					//Check width and height
					if ((current.getWidth() == width) && (current.getHeight() == height)){
						//Select display mode with highest frequency (refresh rate for monitor)
						if((targetDisplayMode == null) || (current.getFrequency() >= freq)){
							//Select with greater bits per pixel (allow for more precise colours)
							if((targetDisplayMode == null) || (current.getBitsPerPixel() > targetDisplayMode.getBitsPerPixel())){
								targetDisplayMode = current;
								freq = targetDisplayMode.getFrequency();
							}
						}

						//Check if current bpp and frequency match the original default mode
						//Ensures maximum compatibility
						if ((current.getBitsPerPixel() == Display.getDesktopDisplayMode().getBitsPerPixel()) &&
								(current.getFrequency() == Display.getDesktopDisplayMode().getFrequency())){
							targetDisplayMode = current;
							break;
						}
					}

				}
			} else { //If not fullscreen
				//No need to query for windowed mode
				targetDisplayMode = new DisplayMode(width, height);
			}

			//If not found
			if (targetDisplayMode == null){
				System.out.println("Failed to find value mode: " + width + "x" + height + " fs=" + fullscreen);
				return false;
			}

			//Set new DisplayMode
			Display.setDisplayMode(targetDisplayMode);
			Display.setFullscreen(fullscreen);

			System.out.println("Selected DisplayMode: " + targetDisplayMode.toString());

			//Generate resized event
			instance.resized();
			return true;
		} catch (LWJGLException e){
			System.out.println("Unable to setup mode " + width + "x" + height + " fullscreen=" + fullscreen + e);
		}
		
		return false;
	}

	//Abstract methods below:

	//Initialisation
	abstract public void init();

	//Update game
	abstract public void update(long elapsedTime);

	//Render game
	abstract public void render();

	//Resize display
	abstract public void resized();

	//Dispose the game
	abstract public void dispose();



}
