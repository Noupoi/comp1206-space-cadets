import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.input.Keyboard.*;

import org.lwjgl.opengl.Display;

public class Part1 extends Game {
    private float x , y; // The position of the rectangle
    private float vX, vY; // Velocity of rectangle
    
	public Part1() {}

	@Override
	public void init() {
		Display.setTitle("Part 1: Creating a moving point");
		
		//Setup OpenGL
		glMatrixMode(GL_PROJECTION); //Set current matrix mode to projection matrix
		glLoadIdentity(); //Load identity matrix
		
		glMatrixMode(GL_MODELVIEW); //Change to model view mode
		glOrtho(0, 800, 600, 0, 1, -1); //Setup orthographic camera, makes it easier for 3D
		glViewport(0, 0, Display.getWidth(), Display.getHeight()); //Set viewport - a window into game
	}

	@Override
	public void update(long elapsedTime) { //Check for input
		if(isKeyDown(KEY_LEFT))
			vX += -1;
		if(isKeyDown(KEY_RIGHT))
			vX += 1;
		if(isKeyDown(KEY_UP))
			vY -= 1;
		if(isKeyDown(KEY_DOWN))
			vY += 1;
		if(isKeyDown(KEY_ESCAPE))
			end();
		
		//Move square
		x += vX;
		y += vY;
		
		//Check if out of bounds
		if(x > 800){
			x = 0;
		} else if (x < 0){
			x = 800;
		}
		
		if (y > 600){
			y = 0;
		} else if ( y < 0) {
			y = 600;
		}
	}

	@Override
	public void render() {
		//Clear screen by clearing colour information - depth does not matter as 2D
		glClear(GL_COLOR_BUFFER_BIT);
		
		//Set colour to white
		glColor3f(1, 1, 1);
		
		//Create a new draw matrix for custom settings
		glPushMatrix();
		{
			//Translate to new new location
			glTranslatef(x, y, 0);
			
			//Draw square
			glBegin(GL_QUADS); //Quadrilateral
			
			{ //Specify vertices to render. Use glVertex2f as this is 2D
				glVertex2f(0, 0); //Top left
				glVertex2f(0, 50); //TR
				glVertex2d(50, 50); //BR
				glVertex2d(50, 0); //BL
			}
			
			glEnd(); //Finish rendering
		}
		
		glPopMatrix(); //Restore original settings
		

	}

	@Override
	public void resized() {
		glViewport(0, 0, Display.getWidth(), Display.getHeight());
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}
	
    public static void main(String[] args){
    	new Part1();
    }

}
